# translation of menu.pl.po to Polish
# Copyright (C) 2007
# This file is distributed under the same license as the menu package.
#
# Wojciech Zareba <wojtekz@comp.waw.pl>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: menu.pl\n"
"Report-Msgid-Bugs-To: menu@packages.debian.org\n"
"POT-Creation-Date: 2007-10-05 07:30+0200\n"
"PO-Revision-Date: 2007-10-09 13:21+0200\n"
"Last-Translator: Wojciech Zareba <wojtekz@comp.waw.pl>\n"
"Language-Team: Polish <debian-l10n-polish@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-2\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: ../install-menu/functions.cc:92
msgid "Zero-size argument to print function."
msgstr "Podano argument o zerowym rozmiarze do funkcji print."

#: ../install-menu/install-menu.cc:202
msgid "install-menu: checking directory %1\n"
msgstr "install-menu: sprawdzanie katalogu %1\n"

#: ../install-menu/install-menu.cc:215
msgid "install-menu: creating directory %1:\n"
msgstr "install-menu: tworzenie katalogu %1:\n"

#: ../install-menu/install-menu.cc:217
msgid "Could not create directory(%1): %2"
msgstr "Nie uda�o si� utworzy� katalogu(%1): %2"

#: ../install-menu/install-menu.cc:219
msgid "Could not change directory(%1): %2"
msgstr "Nie uda�o si� zmieni� katalogu(%1): %2"

#: ../install-menu/install-menu.cc:222
msgid "install-menu: directory %1 already exists\n"
msgstr "install-menu: katalog %1 ju� istnieje\n"

#. Do not translate supported
#: ../install-menu/install-menu.cc:447
msgid "install-menu: [supported]: name=%1\n"
msgstr "install-menu: [supported]: nazwa=%1\n"

#: ../install-menu/install-menu.cc:464
msgid "Menu entry lacks mandatory field \"%1\".\n"
msgstr "Brak wymaganego pola \"%1\" we wpisie menu.\n"

#: ../install-menu/install-menu.cc:470
msgid "Unknown value for field %1=\"%2\".\n"
msgstr "Nieznana warto�� dla pola %1=\"%2\".\n"

#. Do not translate quoted text
#: ../install-menu/install-menu.cc:617
msgid "install-menu: \"hotkeycase\" can only be \"sensitive\" or \"insensitive\"\n"
msgstr ""
"install-menu: \"hotkeycase\" mo�e by� ustawione tylko na \"sensitive\" lub "
"\"insensitive\"\n"

#: ../install-menu/install-menu.cc:647
msgid ""
"install-menu: Warning: Unknown identifier `%1' on line %2 in file %3. "
"Ignoring.\n"
msgstr ""
"install-menu: Ostrze�enie: Nieznany identyfikator `%1' w linii %2 w pliku %3. "
"Zignorowano.\n"

#: ../install-menu/install-menu.cc:657
msgid "install-menu: %1 must be defined in menu-method %2"
msgstr "install-menu: %1 musi by� zdefiniowane w metodzie menu %2"

#: ../install-menu/install-menu.cc:824
msgid "Cannot open file %1 (also tried %2).\n"
msgstr "Nie uda�o si� otworzy� pliku %1 (ani %2).\n"

#: ../install-menu/install-menu.cc:832 ../install-menu/install-menu.cc:839
#: ../install-menu/install-menu.cc:847
msgid "Cannot open file %1.\n"
msgstr "Nie uda�o si� otworzy� pliku %1.\n"

#: ../install-menu/install-menu.cc:849
msgid ""
"In order to be able to create the user config file(s) for the window "
"manager,\n"
"the above file needs to be writeable (and/or the directory needs to exist).\n"
msgstr ""
"Aby utworzy� pliki konfiguracyjne u�ytkownika dla menad�era okien\n"
"potrzebne s� uprawnienia do zapisyania wy�ej wymienionych plik�w\n"
"(i/lub musi istnie� katalog).\n"

#: ../install-menu/install-menu.cc:871
msgid "Warning: the string %1 did not occur in template file %2\n"
msgstr "Ostrze�enie: napis %1 nie wyst�pi� w pliku szablonu %2\n"

#. Don't translate quoted string
#: ../install-menu/install-menu.cc:896
msgid ""
"install-menu [-vh] <menu-method>\n"
"  Read menu entries from stdin in \"update-menus --stdout\" format\n"
"  and generate menu files using the specified menu-method.\n"
"  Options to install-menu:\n"
"     -h --help    : this message\n"
"        --remove  : remove the menu instead of generating it.\n"
"     -v --verbose : be verbose\n"
msgstr ""
"install-menu [-vh] <metoda-menu>\n"
"  Czyta wpisy menu ze standardowego wej�cia w formacie\n"
"  \"update-menus --stdout\" i generuje pliki menu u�ywaj�c okre�lonej metody.\n"
"  Opcje install-menu:\n"
"     -h --help    : ten komunikat\n"
"        --remove  : usuwa menu zamiast tworzy�\n"
"     -v --verbose : tryb gadatliwy\n"

#: ../install-menu/install-menu.cc:943
msgid "install-menu: no menu-method script specified!"
msgstr "install-menu: nie okre�lono skryptu metody menu!"

#: ../install-menu/install-menu.cc:956
msgid "Cannot open script %1 for reading.\n"
msgstr "Nie uda�o si� otworzy� skryptu %1 do odczytu.\n"

#: ../install-menu/install-menu.cc:979
msgid "Warning: script %1 does not provide removemenu, menu not deleted\n"
msgstr "Ostrze�enie: skrypt %1 nie umo�liwia usuwania menu, menu nie skasowane\n"

#: ../install-menu/install-menu.cc:1014
msgid "Running: \"%1\"\n"
msgstr "Uruchamianie: \"%1\"\n"

#: ../install-menu/install-menu.cc:1030
msgid "install-menu: %1: aborting\n"
msgstr "install-menu: %1: przerwanie dzia�ania\n"

#: ../install-menu/install-menu.h:199
msgid "Number of arguments to function %1 does not match."
msgstr "Nieprawid�owa liczba argument�w funkcji %1."

#: ../install-menu/install-menu.h:207
msgid "Unknown function: \"%1\""
msgstr "Nieznana funkcja: \"%1\""

#: ../install-menu/install-menu.h:216
msgid "Indirectly used, but not defined function: \"%1\""
msgstr "Po�rednio u�yta, ale niezdefiniowana funkcja: \"%1\""

#: ../install-menu/install-menu.h:225
msgid "Unknown identifier: \"%1\""
msgstr "Nieznany identyfikator: \"%1\""

#: ../install-menu/install-menu.h:234
msgid "Encoding conversion error: \"%1\""
msgstr "B��d konwersji kodowania: \"%1\""

#: ../update-menus/exceptions.h:43
msgid "Unknown error."
msgstr "B��dny b��d."

#: ../update-menus/exceptions.h:55 ../update-menus/parsestream.h:114
msgid "Unknown error, message=%1"
msgstr "Nieznany b��d, komunikat=%1"

#: ../update-menus/exceptions.h:64
msgid "Unable to open file \"%1\"."
msgstr "Nie uda�o si� otworzy� pliku \"%1\"."

#: ../update-menus/exceptions.h:73
msgid "Failed to pipe data through \"%1\" (pipe opened for reading)."
msgstr "Nie uda�o si� przes�a� danych przez \"%1\" (potok otwarty do czytania)."

#: ../update-menus/exceptions.h:89
msgid "%1: missing required tag: \"%2\""
msgstr "%1: brak wymaganego tagu: \"%2\""

#: ../update-menus/parsestream.cc:50
msgid "(probably) stdin"
msgstr "(prawdopodobnie) standardowe wej�cie"

#: ../update-menus/parsestream.cc:450
msgid "In file \"%1\", at (or in the definition that ends at) line %2:\n"
msgstr "W pliku \"%1\", w linii (lub w definicji ko�cz�cej si� w l.) %2:\n"

#: ../update-menus/parsestream.cc:471
msgid "Somewhere in input file:\n"
msgstr "Gdzie� w pliku wej�ciowym:\n"

#: ../update-menus/parsestream.h:122
msgid "Unexpected end of file."
msgstr "Niespodziewany koniec pliku."

#: ../update-menus/parsestream.h:129
msgid "Unexpected end of line."
msgstr "Niespodziewany koniec linii."

#: ../update-menus/parsestream.h:136
msgid "Identifier expected."
msgstr "Brak spodziewanego identyfikatora."

#: ../update-menus/parsestream.h:144
msgid "Expected: \"%1\""
msgstr "Spodziewano si� \"%1\""

#: ../update-menus/parsestream.h:153
msgid "Unexpected character: \"%1\""
msgstr "Niespodziewany znak: \"%1\""

#: ../update-menus/parsestream.h:162
msgid ""
"Boolean (either true or false) expected.\n"
"Found: \"%1\""
msgstr ""
"Spodziewano si� warto�ci logicznej (true lub false).\n"
"Znaleziono: \"%1\""

#: ../update-menus/parsestream.h:172
msgid "Unknown compat mode: \"%1\""
msgstr "Nieznany tryb zgodno�ci: \"%1\""

#: ../update-menus/stringtoolbox.cc:85
msgid ""
"replacewith($string, $replace, $with): $replace and $with must have the same "
"length."
msgstr "replacewith($napis, $co, $czym): $co i $czym musz� mie� identyczn� d�ugo��."

#: ../update-menus/update-menus.cc:150
msgid ""
"file %1 line %2:\n"
"Discarding entry requiring missing package %3."
msgstr ""
"plik %1 linia %2:\n"
"Wpis odrzucony z powodu braku pakietu %3."

#. Translation here and below refer to the file
#. /etc/menu-methods/translate_menus that allow to rename and reorganize
#. menu entries automatically. It does not refer to the localisation
#. (translation to other languages).
#.
#: ../update-menus/update-menus.cc:364
msgid "Reading translation rules in %1."
msgstr "Wczytywanie regu� organizacji menu z %1."

#: ../update-menus/update-menus.cc:444
msgid "Reading installed packages list..."
msgstr "Czytanie listy zainstalowanych pakiet�w..."

#: ../update-menus/update-menus.cc:493
msgid "Execution of %1 generated no output or returned an error.\n"
msgstr "Pr�ba wykonania %1 nie wygenerowa�a danych lub zwr�ci�a b��d.\n"

#: ../update-menus/update-menus.cc:534 ../update-menus/update-menus.cc:538
msgid "Skipping file because of errors...\n"
msgstr "Plik pomini�ty z powodu b��d�w...\n"

#: ../update-menus/update-menus.cc:555
msgid "Reading menu-entry files in %1."
msgstr "Czytanie plik�w wpis�w menu z %1."

#: ../update-menus/update-menus.cc:578
msgid "Error reading %1.\n"
msgstr "B��d odczytu %1.\n"

#: ../update-menus/update-menus.cc:584
msgid "%1 menu entries found (%2 total)."
msgstr "Znalezionych wpis�w menu: %1 (w sumie %2)."

#: ../update-menus/update-menus.cc:595
msgid "Running method: %1 --remove"
msgstr "Uruchomiona metoda: %1 --remove"

#: ../update-menus/update-menus.cc:607 ../update-menus/update-menus.cc:669
#: ../update-menus/update-menus.cc:990
msgid "Script %1 could not be executed."
msgstr "Nie uda�o si� wykona� skryptu %1."

#: ../update-menus/update-menus.cc:610 ../update-menus/update-menus.cc:672
msgid "Script %1 returned error status %2."
msgstr "Skrypt %1 zwr�ci� kod b��du %2."

#: ../update-menus/update-menus.cc:613 ../update-menus/update-menus.cc:675
msgid "Script %1 received signal %2."
msgstr "Skrypt %1 odebra� sygna� %2."

#: ../update-menus/update-menus.cc:630
msgid "Running method: %1"
msgstr "Uruchomiona metoda: %1"

#: ../update-menus/update-menus.cc:633
msgid "Cannot create pipe."
msgstr "Nie uda�o si� utworzy� potoku."

#: ../update-menus/update-menus.cc:685
msgid "Running menu-methods in %1."
msgstr "Wykonywanie metod menu w %1."

#: ../update-menus/update-menus.cc:717
msgid "Other update-menus processes are already locking %1, quitting."
msgstr "Inne procesy update-menus blokuj� ju� %1, wyj�cie."

#: ../update-menus/update-menus.cc:720
msgid "Cannot lock %1: %2 - Aborting."
msgstr "Nie uda�o si� zablokowa� %1: %2 - przerwanie dzia�ania."

#: ../update-menus/update-menus.cc:730
msgid "Cannot write to lockfile %1 - Aborting."
msgstr "Nie uda�o si� zapisa� do pliku blokady %1 - przerwanie dzia�ania."

#: ../update-menus/update-menus.cc:743
msgid "Cannot remove lockfile %1."
msgstr "Nie uda�o si� usun�� pliku blokady %1."

#: ../update-menus/update-menus.cc:759
msgid "Update-menus is run by user."
msgstr "Update-menus zosta� uruchomiony przez u�ytkownika."

#: ../update-menus/update-menus.cc:813
msgid ""
"Waiting for dpkg to finish (forking to background).\n"
"(checking %1)"
msgstr ""
"Oczekiwanie na zako�czenia dzia�ania dpkg (przej�cie w t�o).\n"
"(sprawdzanie %1)"

#: ../update-menus/update-menus.cc:816
msgid "Further output (if any) will appear in %1."
msgstr "Wyniki przetwarzania pojawi� si� w %1 (o ile b�d�)."

#: ../update-menus/update-menus.cc:845
msgid "Dpkg is not locking dpkg status area, good."
msgstr "Dpkg nie blokuje obszaru stanu, jest dobrze."

#. This is the update-menus --help message
#: ../update-menus/update-menus.cc:855
msgid ""
"Usage: update-menus [options] \n"
"Gather packages data from the menu database and generate menus for\n"
"all programs providing menu-methods, usually window-managers.\n"
"  -d                     Output debugging messages.\n"
"  -v                     Be verbose about what is going on.\n"
"  -h, --help             This message.\n"
"  --menufilesdir=<dir>   Add <dir> to the lists of menu directories to "
"search.\n"
"  --menumethod=<method>  Run only the menu method <method>.\n"
"  --nodefaultdirs        Disable the use of all the standard menu "
"directories.\n"
"  --nodpkgcheck          Do not check if packages are installed.\n"
"  --remove               Remove generated menus instead.\n"
"  --stdout               Output menu list in format suitable for piping to\n"
"                         install-menu.\n"
"  --version              Output version information and exit.\n"
msgstr ""
"U�ycie: update-menus [opcje] \n"
"Pobiera dane o pakietach z bazy danych menu i generuje menu dla wszystkich\n"
"program�w dostarczaj�cych \"metod menu\", zazwyczaj mened�er�w okien.\n"
"  -d                       Wy�wietla informacje diagnostyczne.\n"
"  -v                       Wy�wietla szczeg�owe informacje.\n"
"  -h, --help               Ten komunikat.\n"
"  --menufilesdir=<katalog> Dodaje <katalog> do listy katalog�w do przeszukania.\n"
"  --menumethod=<metoda>    Uruchamia tylko metod� <metoda>.\n"
"  --nodefaultdirs          Pomija standardowe katalogi menu.\n"
"  --nodpkgcheck            Nie sprawdza, czy pakiety s� zainstalowane.\n"
"  --remove                 Nie tworzy, tylko usuwa menu.\n"
"  --stdout                 Wy�wietla list� menu w formacie odpowiednim do"
"                           przekazania do install-menu.\n"
"  --version                Wy�wietla informacje o wersji i wychodzi.\n"

#: ../update-menus/update-menus.h:151
msgid "Unknown install condition \"%1\" (currently, only \"package\" is supported)."
msgstr "Nieznany warunek instalacji \"%1\" (obecnie obs�ugiwany jest tylko \"package\")."

